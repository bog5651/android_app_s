package com.example.stas.android_app_s.Activitys;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.stas.android_app_s.Fragments.FAbout;
import com.example.stas.android_app_s.Fragments.FCycle;
import com.example.stas.android_app_s.Fragments.FDiscipline;
import com.example.stas.android_app_s.Helpers.RequestHelper;
import com.example.stas.android_app_s.Helpers.RequestHelper.ApiInterface;
import com.example.stas.android_app_s.Helpers.SharedPreferencesHelper;
import com.example.stas.android_app_s.R;

public class MainApp extends AppCompatActivity {

    private static final String TAG = "MainApp";

    private DrawerLayout drawerLayout;
    private FrameLayout navView;

    private TextView tvLogin;

    private Button btnExit;
    private Button btnCycle;
    private Button btnDiscipline;

    private Integer container_id = R.id.container;
    private Context context = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_main);
        RequestHelper.init(context);

        drawerLayout = findViewById(R.id.drawer_layout);
        navView = findViewById(R.id.nav_view);

        tvLogin = findViewById(R.id.tvLogin);

        btnExit = findViewById(R.id.btnExit);
        btnCycle = findViewById(R.id.btnCycle);
        btnDiscipline = findViewById(R.id.btnDiscipline);

        //init
        tvLogin.setText(SharedPreferencesHelper.getLogin(context));

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestHelper.apiLogout(new ApiInterface.onComplete() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "Logout onSuccess: ");
                        SharedPreferencesHelper.logout(context);
                        Intent intent = new Intent(context, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                        startActivity(intent);
                    }

                    @Override
                    public void onFail(String error) {
                        Log.d(TAG, "Logout onSuccess: ");
                        SharedPreferencesHelper.logout(context);
                        Intent intent = new Intent(context, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                        startActivity(intent);
                    }
                });
            }
        });

        btnCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().add(container_id, new FCycle(), "frag").commit();
                drawerLayout.closeDrawer(Gravity.START);
            }
        });

        btnDiscipline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().add(container_id, new FDiscipline(), "frag").commit();
                drawerLayout.closeDrawer(Gravity.START);
            }
        });

        getFragmentManager().beginTransaction().add(container_id, new FAbout(), "frag").commit();

    }
}
