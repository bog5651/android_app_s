package com.example.stas.android_app_s.Activitys;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.stas.android_app_s.Helpers.RequestHelper;
import com.example.stas.android_app_s.Objects.User;
import com.example.stas.android_app_s.R;

public class RegisterActivity extends AppCompatActivity {

    private EditText etLogin;
    private EditText etPassword;
    private EditText etEmail;

    private Button btnRegister;

    private Context context = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_register);
        RequestHelper.init(this);

        etLogin = findViewById(R.id.etLogin);
        etPassword = findViewById(R.id.etPassword);
        etEmail = findViewById(R.id.etEmail);

        btnRegister = findViewById(R.id.btnRegister);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User();
                user.login = etLogin.getText().toString().trim();
                user.password = etPassword.getText().toString().trim();
                user.email = etEmail.getText().toString().trim();

                RequestHelper.apiRegister(user, new RequestHelper.ApiInterface.onComplete() {
                    @Override
                    public void onSuccess() {
                        Intent toAct = new Intent(context, MainApp.class);
                        startActivity(toAct);
                    }

                    @Override
                    public void onFail(String error) {
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}
