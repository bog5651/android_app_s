package com.example.stas.android_app_s.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.stas.android_app_s.Objects.Cycle;
import com.example.stas.android_app_s.R;

import java.util.ArrayList;

public class CycleAdapter extends ArrayAdapter implements View.OnClickListener {

    private ArrayList<Cycle> cycles;

    private Context context;
    private onItemRemove callBack;

    public boolean isEdit;

    public CycleAdapter(@NonNull Context context, ArrayList<Cycle> cycles, onItemRemove callback) {
        super(context, R.layout.cycle_item, cycles);
        this.context = context;
        this.cycles = cycles;
        this.callBack = callback;
    }

    @Override
    public void remove(@Nullable Object object) {
        super.remove(object);
    }

    public void remove(int position) {
        super.remove(getItem(position));
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return cycles.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = null;
        if (inflater != null) {
            rowView = inflater.inflate(R.layout.cycle_item, parent, false);
        }
        Cycle itemInfo = (Cycle) getItem(position);
        if (rowView != null) {
            TextView tvProductName = rowView.findViewById(R.id.tvName);
            tvProductName.setText(String.valueOf(itemInfo.cycle_name));

            ImageView imageView = rowView.findViewById(R.id.imageButton);
            imageView.setOnClickListener(this);
            imageView.setTag(position);
            if (isEdit)
                imageView.setVisibility(View.VISIBLE);
            else
                imageView.setVisibility(View.GONE);
        } else {
            rowView = new View(context);
        }
        return rowView;
    }

    @Override
    public void add(@Nullable Object object) {
        super.add(object);
        //cycles.add((Cycle) object);
    }

    @Override
    public void onClick(View v) {
        if (callBack != null) {
            callBack.onDelete((int) v.getTag());
        }
    }

    public interface onItemRemove {
        void onDelete(int position);
    }
}
