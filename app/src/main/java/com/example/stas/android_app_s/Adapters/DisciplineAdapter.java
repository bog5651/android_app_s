package com.example.stas.android_app_s.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.stas.android_app_s.Objects.Cycle;
import com.example.stas.android_app_s.Objects.Discipline;
import com.example.stas.android_app_s.R;

import java.util.ArrayList;

public class DisciplineAdapter extends ArrayAdapter implements View.OnClickListener {

    private ArrayList<Discipline> disciplines;

    private Context context;
    private onItemRemove callBack;

    public boolean isEdit;

    public DisciplineAdapter(@NonNull Context context,ArrayList<Discipline> disciplines, onItemRemove callback) {
        super(context, R.layout.cycle_item, disciplines);
        this.context = context;
        this.disciplines = disciplines;
        this.callBack = callback;
    }

    @Override
    public void remove(@Nullable Object object) {
        super.remove(object);
    }

    public void remove(int position) {
        super.remove(getItem(position));
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return disciplines.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = null;
        if (inflater != null) {
            rowView = inflater.inflate(R.layout.discipline_item, parent, false);
        }
        Discipline itemInfo = (Discipline) getItem(position);
        if (rowView != null) {
            TextView tvName = rowView.findViewById(R.id.tvName);
            tvName.setText(String.valueOf(itemInfo.d_name));

            TextView tvSemester = rowView.findViewById(R.id.tvSemester);
            tvSemester.setText(String.valueOf(itemInfo.semester));

            TextView tvSchool_hours = rowView.findViewById(R.id.tvSchool_hours);
            tvSchool_hours.setText(String.valueOf(itemInfo.school_hours));

            TextView tvD_cycle_ID = rowView.findViewById(R.id.tvD_cycle_ID);
            tvD_cycle_ID.setText(String.valueOf(itemInfo.d_cycle_ID));

            ImageView imageView = rowView.findViewById(R.id.imageButton);
            imageView.setOnClickListener(this);
            imageView.setTag(position);
            if (isEdit)
                imageView.setVisibility(View.VISIBLE);
            else
                imageView.setVisibility(View.GONE);
        } else {
            rowView = new View(context);
        }
        return rowView;
    }

    @Override
    public void add(@Nullable Object object) {
        super.add(object);
    }

    @Override
    public void onClick(View v) {
        if (callBack != null) {
            callBack.onDelete((int) v.getTag());
        }
    }

    public interface onItemRemove {
        void onDelete(int position);
    }
}
