package com.example.stas.android_app_s.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.stas.android_app_s.Objects.Cycle;
import com.example.stas.android_app_s.R;

public class CycleDialog extends Dialog {

    private Context context;

    private EditText etCycleName;
    private Button btnAdd;

    private onButtonPress listener;

    public interface onButtonPress {
        void onPressAdd(Cycle cycle);

        void onPressCancel();
    }

    public CycleDialog(@NonNull Context context, onButtonPress listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cycle_dialog);

        etCycleName = findViewById(R.id.etName);
        btnAdd = findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etCycleName.getText().toString().trim().equals("")) {
                    Cycle cycle = new Cycle();
                    cycle.cycle_name = etCycleName.getText().toString().trim();
                    listener.onPressAdd(cycle);
                } else {
                    Toast.makeText(context, "Введите имя", Toast.LENGTH_LONG).show();
                }
                dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        listener.onPressCancel();
    }
}
