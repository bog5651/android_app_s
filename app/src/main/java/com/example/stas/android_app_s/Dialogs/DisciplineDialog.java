package com.example.stas.android_app_s.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.stas.android_app_s.Helpers.RequestHelper;
import com.example.stas.android_app_s.Objects.Cycle;
import com.example.stas.android_app_s.Objects.Discipline;
import com.example.stas.android_app_s.R;

import java.util.ArrayList;

public class DisciplineDialog extends Dialog {

    private Context context;

    private EditText etName;
    private EditText etSemester;
    private EditText etShHour;
    private Button btnAdd;

    private Spinner spinner;

    private onButtonPress listener;

    public interface onButtonPress {
        void onPressAdd(Discipline discipline);

        void onPressCancel();
    }

    public DisciplineDialog(@NonNull Context context, onButtonPress listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discipline_dialog);

        etName = findViewById(R.id.etName);
        etSemester = findViewById(R.id.etSemester);
        etShHour = findViewById(R.id.etShHour);

        btnAdd = findViewById(R.id.btnAdd);

        spinner = findViewById(R.id.spinner);

        RequestHelper.apiGetCycle(null, new RequestHelper.ApiInterface.onCompleteGetCycle() {
            @Override
            public void onSuccess(ArrayList<Cycle> cycles) {
                spinner.setAdapter(new MyAdapter(context, cycles));
            }

            @Override
            public void onFail(String error) {
                Toast.makeText(context, error, Toast.LENGTH_LONG).show();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etName.getText().toString().trim().equals("")
                        && !etSemester.getText().toString().trim().equals("")
                        && !etShHour.getText().toString().trim().equals("")) {

                    Discipline discipline = new Discipline();

                    discipline.d_name = etName.getText().toString().trim();
                    discipline.semester = Integer.parseInt(etSemester.getText().toString().trim());
                    discipline.school_hours = Integer.parseInt(etShHour.getText().toString().trim());
                    discipline.d_cycle_ID = ((Cycle) spinner.getSelectedItem()).id;

                    listener.onPressAdd(discipline);
                } else {
                    Toast.makeText(context, "Заполните все поля", Toast.LENGTH_LONG).show();
                }
                dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        listener.onPressCancel();
    }

    private class MyAdapter extends ArrayAdapter<Cycle> {

        private ArrayList<Cycle> cycles;

        private Context context;

        public MyAdapter(@NonNull Context context, @NonNull ArrayList<Cycle> objects) {
            super(context, R.layout.spinner_item, objects);
            this.cycles = objects;
            this.context = context;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = null;
            if (inflater != null) {
                rowView = inflater.inflate(R.layout.spinner_item, parent, false);
            }
            Cycle itemInfo = getItem(position);
            if (rowView != null) {
                TextView tvName = rowView.findViewById(R.id.tvName);
                tvName.setText(String.valueOf(itemInfo.cycle_name));
            } else {
                rowView = new View(context);
            }
            return rowView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = null;
            if (inflater != null) {
                rowView = inflater.inflate(R.layout.spinner_item, parent, false);
            }
            Cycle itemInfo = getItem(position);
            if (rowView != null) {
                TextView tvName = rowView.findViewById(R.id.tvName);
                tvName.setText(String.valueOf(itemInfo.cycle_name));
            } else {
                rowView = new View(context);
            }
            return rowView;
        }

        @Nullable
        @Override
        public Cycle getItem(int position) {
            return cycles.get(position);
        }
    }
}
