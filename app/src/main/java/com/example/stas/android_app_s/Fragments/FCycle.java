package com.example.stas.android_app_s.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.stas.android_app_s.Adapters.CycleAdapter;
import com.example.stas.android_app_s.Dialogs.CycleDialog;
import com.example.stas.android_app_s.Helpers.RequestHelper;
import com.example.stas.android_app_s.Objects.Cycle;
import com.example.stas.android_app_s.R;

import java.util.ArrayList;

public class FCycle extends Fragment {

    private static final String TAG = "FCYCLE";

    private ListView lvList;

    private TextView tvName;
    private TextView tvSemester;
    private TextView tvSchool_hours;
    private TextView tvD_cycle_ID;

    private Switch aSwitch;
    private Button btnAdd;

    private CycleAdapter adapter;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fr_list, null);

        tvName = root.findViewById(R.id.tvName);
        tvSemester = root.findViewById(R.id.tvSemester);
        tvSchool_hours = root.findViewById(R.id.tvSchool_hours);
        tvD_cycle_ID = root.findViewById(R.id.tvD_cycle_ID);

        tvName.setText("Цикл");
        tvSemester.setText("");
        tvSchool_hours.setText("");
        tvD_cycle_ID.setText("");

        context = root.getContext();
        lvList = root.findViewById(R.id.lvList);

        aSwitch = root.findViewById(R.id.swEdit);
        btnAdd = root.findViewById(R.id.btnAdd);

        RequestHelper.apiGetCycle(null, new RequestHelper.ApiInterface.onCompleteGetCycle() {
            @Override
            public void onSuccess(ArrayList<Cycle> cycles) {
                Log.d(TAG, "onSuccess: " + cycles.size());
                adapter = new CycleAdapter(context, cycles, new CycleAdapter.onItemRemove() {
                    @Override
                    public void onDelete(final int position) {
                        Cycle cycle = (Cycle) adapter.getItem(position);
                        RequestHelper.apiRemoveCycle(cycle.id, new RequestHelper.ApiInterface.onComplete() {
                            @Override
                            public void onSuccess() {
                                adapter.remove(position);
                                adapter.notifyDataSetChanged();
                                adapter.notifyDataSetInvalidated();
                            }

                            @Override
                            public void onFail(String error) {
                                Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
                lvList.setAdapter(adapter);
            }

            @Override
            public void onFail(String error) {
                Log.d(TAG, "onSuccess: " + error);
            }
        });

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                adapter.isEdit = isChecked;
                if (isChecked)
                    btnAdd.setVisibility(View.VISIBLE);
                else
                    btnAdd.setVisibility(View.GONE);
                adapter.notifyDataSetInvalidated();
                adapter.notifyDataSetChanged();
            }
        });

        btnAdd.setVisibility(View.GONE);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CycleDialog dialog = new CycleDialog(context, new CycleDialog.onButtonPress() {
                    @Override
                    public void onPressAdd(final Cycle cycle) {
                        RequestHelper.apiAddCycle(cycle, new RequestHelper.ApiInterface.onComplete() {
                            @Override
                            public void onSuccess() {
                                adapter.add(cycle);
                                Toast.makeText(context, "Цикл успешно добавлен", Toast.LENGTH_LONG).show();
                                adapter.notifyDataSetInvalidated();
                                adapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onFail(String error) {
                                Toast.makeText(context, "Цикл не добавлен по ошибке \n" + error, Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    @Override
                    public void onPressCancel() {

                    }
                });
                dialog.show();
            }
        });

        return root;
    }
}
