package com.example.stas.android_app_s.Helpers;

import android.content.Context;
import android.util.Log;

import com.example.stas.android_app_s.Objects.Cycle;
import com.example.stas.android_app_s.Objects.Discipline;
import com.example.stas.android_app_s.Objects.User;
import com.example.stas.android_app_s.utils.GetterJSON;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RequestHelper {

    private static final String TAG = "requestHelper";

    private static final String host = "http://192.168.0.62"; //локальная сеть
    //private static final String host = "http://192.168.33.10"; //локальная машина

    private static final String UrlLogin = "/api/login";
    private static final String UrlUser = "/api/user";
    private static final String UrlRegister = "/api/registration";
    private static final String UrlLogout = "/api/logout";

    private static final String UrlCycle = "/api/cycle";
    private static final String UrlRemoveCycle = "/api/removecycle";
    private static final String UrlAddCycle = "/api/addcycle";
    //private static final String UrlUpdateCycle = "/api/updatecycle";

    private static final String UrlDiscipline = "/api/discipline";
    private static final String UrlRemoveDiscipline = "/api/removediscipline";
    private static final String UrlAddDiscipline = "/api/adddiscipline";
    private static final String UrlUpdateDiscipline = "/api/updatediscipline";

    private static Context mContext;
    private static GsonBuilder builder = new GsonBuilder();

    public static void init(Context context) {
        mContext = context;
    }

    public static void apiLogin(final String login, final String password, final ApiInterface.onComplete callback) {
        GetterJSON getter = new GetterJSON(new GetterJSON.onCompleteEventHandler() {
            @Override
            public void onComplete(String Json) {
                if (Json == null) {
                    callback.onFail("Internet Error");
                    return;
                }
                try {
                    JSONObject result = new JSONObject(Json);
                    if (result.getInt("success") == 1) {
                        String token = result.getString("token");
                        SharedPreferencesHelper.putToken(mContext, token);
                        SharedPreferencesHelper.putLogin(mContext, login);
                        SharedPreferencesHelper.putPassword(mContext, password);
                        callback.onSuccess();
                    } else {
                        callback.onFail(formatError(result.getJSONObject("error")));
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "onCompleteWithResult: exn " + e.getMessage());
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
        JSONObject JSONtoSend = new JSONObject();
        try {
            JSONtoSend.put("login", login);
            JSONtoSend.put("password", password);
        } catch (JSONException e) {
            Log.e(TAG, "onClick: exn " + e.getMessage());
            callback.onFail(e.getMessage());
        }
        getter.execute(host + UrlLogin, JSONtoSend.toString());
    }

    public static void apiGetUser(final ApiInterface.onCompleteGetUser callback) {
        getToken(new onGetTokenListener() {
            @Override
            public void onGetToken(String token) {
                GetterJSON getter = new GetterJSON(new GetterJSON.onCompleteEventHandler() {
                    @Override
                    public void onComplete(String Json) {
                        if (Json == null) {
                            callback.onFail("Internet Error");
                            return;
                        }
                        try {
                            JSONObject result = new JSONObject(Json);
                            if (result.getInt("success") == 1) {
                                JSONObject jsonUser = result.getJSONObject("user");
                                Gson gson = builder.create();
                                User user = gson.fromJson(jsonUser.toString(), User.class);
                                callback.onSuccess(user);
                            } else {
                                callback.onFail(formatError(result.getJSONObject("error")));
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "onCompleteWithResult: " + e.getMessage());
                            callback.onFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onFail(String error) {
                        callback.onFail(error);
                    }
                });
                JSONObject JSONtoSend = new JSONObject();
                try {
                    JSONtoSend.put("token", token);
                } catch (JSONException e) {
                    Log.e(TAG, "onClick: exn " + e.getMessage());
                }
                getter.execute(host + UrlUser, JSONtoSend.toString());
            }

            @Override
            public void onFail() {
                callback.onFail("Ошибка получения токена, пожалуйста, залогиньтесь");
            }
        });
    }

    public static void apiRegister(final User user, final ApiInterface.onComplete callback) {
        GetterJSON getterJSON = new GetterJSON(new GetterJSON.onCompleteEventHandler() {
            @Override
            public void onComplete(String Json) {
                if (Json == null) {
                    callback.onFail("Internet Error");
                    return;
                }
                try {
                    JSONObject result = new JSONObject(Json);
                    if (result.getInt("success") == 1) {
                        String token = result.getString("token");
                        if (!token.equals("")) {
                            SharedPreferencesHelper.putToken(mContext, token);
                            SharedPreferencesHelper.putLogin(mContext, user.login);
                            SharedPreferencesHelper.putPassword(mContext, user.password);
                            callback.onSuccess();
                        } else
                            callback.onFail("Вернулся пустой токен");
                    } else {
                        callback.onFail(formatError(result.getJSONObject("error")));
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "onFail: " + e.getMessage());
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
        Gson gson = builder.create();
        getterJSON.execute(host + UrlRegister, gson.toJson(user));
    }

    public static void apiLogout(final ApiInterface.onComplete callback) {
        getToken(new onGetTokenListener() {
            @Override
            public void onGetToken(String token) {
                GetterJSON getter = new GetterJSON(new GetterJSON.onCompleteEventHandler() {
                    @Override
                    public void onComplete(String Json) {
                        if (Json == null) {
                            callback.onFail("Internet Error");
                            return;
                        }
                        try {
                            JSONObject result = new JSONObject(Json);
                            if (result.getInt("success") == 1) {
                                callback.onSuccess();
                            } else {
                                callback.onFail(formatError(result.getJSONObject("error")));
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "onCompleteWithResult: exn " + e.getMessage());
                            callback.onFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onFail(String error) {
                        callback.onFail(error);
                    }
                });
                JSONObject JSONtoSend = new JSONObject();
                try {
                    JSONtoSend.put("token", token);
                } catch (JSONException e) {
                    Log.e(TAG, "onClick: exn " + e.getMessage());
                    callback.onFail(e.getMessage());
                }
                getter.execute(host + UrlLogout, JSONtoSend.toString());
            }

            @Override
            public void onFail() {
                callback.onFail("Ошибка получения токена, пожалуйста, залогиньтесь");
            }
        });
    }

    public static void apiGetCycle(final Integer id, final ApiInterface.onCompleteGetCycle callback) {
        getToken(new onGetTokenListener() {
            @Override
            public void onGetToken(String token) {
                GetterJSON getter = new GetterJSON(new GetterJSON.onCompleteEventHandler() {
                    @Override
                    public void onComplete(String Json) {
                        if (Json == null) {
                            callback.onFail("Internet Error");
                            return;
                        }
                        try {
                            JSONObject result = new JSONObject(Json);
                            if (result.getInt("success") == 1) {
                                JSONArray jsonCycles = result.getJSONArray("cycles");
                                ArrayList<Cycle> cycles = new ArrayList<>();
                                Gson gson = builder.create();
                                for (int i = 0; i < jsonCycles.length(); i++) {
                                    cycles.add(gson.fromJson(jsonCycles.get(i).toString(), Cycle.class));
                                }
                                callback.onSuccess(cycles);
                            } else {
                                Log.d(TAG, "onComplete: " + result.toString());
                                callback.onFail(formatError(result.getJSONObject("error")));
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "apiGetCycle: exn " + e.getMessage());
                            callback.onFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onFail(String error) {
                        callback.onFail(error);
                    }
                });
                JSONObject JSONtoSend = new JSONObject();
                try {
                    JSONtoSend.put("token", token);
                } catch (JSONException e) {
                    Log.e(TAG, "onClick: exn " + e.getMessage());
                    callback.onFail(e.getMessage());
                }
                if (id != null) {
                    try {
                        JSONtoSend.put("cycle", new JSONObject("'id':" + id.toString()));
                    } catch (JSONException e) {
                        Log.e(TAG, "onClick: exn " + e.getMessage());
                        callback.onFail(e.getMessage());
                    }
                } else {
                    try {
                        JSONtoSend.put("cycle", new JSONObject());
                    } catch (JSONException e) {
                        Log.e(TAG, "onClick: exn " + e.getMessage());
                        callback.onFail(e.getMessage());
                    }
                }
                getter.execute(host + UrlCycle, JSONtoSend.toString());
            }

            @Override
            public void onFail() {
                callback.onFail("Ошибка получения токена");
            }
        });
    }

    public static void apiAddCycle(final Cycle cycle, final ApiInterface.onComplete callback) {
        getToken(new onGetTokenListener() {
            @Override
            public void onGetToken(String token) {
                GetterJSON getter = new GetterJSON(new GetterJSON.onCompleteEventHandler() {
                    @Override
                    public void onComplete(String Json) {
                        if (Json == null) {
                            callback.onFail("Internet Error");
                            return;
                        }
                        try {
                            JSONObject result = new JSONObject(Json);
                            if (result.getInt("success") == 1) {
                                callback.onSuccess();
                            } else {
                                Log.d(TAG, "onComplete: " + result.toString());
                                callback.onFail(formatError(result.getJSONObject("error")));
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "apiGetCycle: exn " + e.getMessage());
                            callback.onFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onFail(String error) {
                        callback.onFail(error);
                    }
                });
                Gson gson = builder.create();
                JSONObject JSONtoSend = new JSONObject();
                try {
                    JSONtoSend.put("token", token);
                    JSONtoSend.put("cycle", new JSONObject(gson.toJson(cycle)));
                } catch (JSONException e) {
                    Log.e(TAG, "onClick: exn " + e.getMessage());
                    callback.onFail(e.getMessage());
                }
                Log.d(TAG, "onGetToken: " + JSONtoSend.toString());
                getter.execute(host + UrlAddCycle, JSONtoSend.toString());
            }

            @Override
            public void onFail() {
                callback.onFail("Ошибка получения токена");
            }
        });
    }

    public static void apiRemoveCycle(final Integer id, final ApiInterface.onComplete callback) {
        getToken(new onGetTokenListener() {
            @Override
            public void onGetToken(String token) {
                GetterJSON getter = new GetterJSON(new GetterJSON.onCompleteEventHandler() {
                    @Override
                    public void onComplete(String Json) {
                        if (Json == null) {
                            callback.onFail("Internet Error");
                            return;
                        }
                        try {
                            JSONObject result = new JSONObject(Json);
                            if (result.getInt("success") == 1) {
                                callback.onSuccess();
                            } else {
                                Log.d(TAG, "onComplete: " + result.toString());
                                callback.onFail(formatError(result.getJSONObject("error")));
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "apiGetCycle: exn " + e.getMessage());
                            callback.onFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onFail(String error) {
                        callback.onFail(error);
                    }
                });

                JSONObject JSONtoSend = new JSONObject();
                try {
                    JSONtoSend.put("token", token);
                    JSONObject jsonCycle = new JSONObject();
                    jsonCycle.put("id", id);
                    JSONtoSend.put("cycle", jsonCycle);
                } catch (JSONException e) {
                    Log.e(TAG, "onClick: exn " + e.getMessage());
                    callback.onFail(e.getMessage());
                }
                Log.d(TAG, "onGetToken: " + JSONtoSend.toString());
                getter.execute(host + UrlRemoveCycle, JSONtoSend.toString());
            }

            @Override
            public void onFail() {
                callback.onFail("ошибка получения токена");
            }
        });
    }

    public static void apiAddDiscipline(final Discipline discipline, final ApiInterface.onComplete callback) {
        getToken(new onGetTokenListener() {
            @Override
            public void onGetToken(String token) {
                GetterJSON getter = new GetterJSON(new GetterJSON.onCompleteEventHandler() {
                    @Override
                    public void onComplete(String Json) {
                        if (Json == null) {
                            callback.onFail("Internet Error");
                            return;
                        }
                        try {
                            JSONObject result = new JSONObject(Json);
                            if (result.getInt("success") == 1) {
                                callback.onSuccess();
                            } else {
                                Log.d(TAG, "onComplete: " + result.toString());
                                callback.onFail(formatError(result.getJSONObject("error")));
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "apiGetCycle: exn " + e.getMessage());
                            callback.onFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onFail(String error) {
                        callback.onFail(error);
                    }
                });
                Gson gson = builder.create();
                JSONObject JSONtoSend = new JSONObject();
                try {
                    JSONtoSend.put("token", token);
                    JSONtoSend.put("discipline", new JSONObject(gson.toJson(discipline)));
                } catch (JSONException e) {
                    Log.e(TAG, "onClick: exn " + e.getMessage());
                    callback.onFail(e.getMessage());
                }
                Log.d(TAG, "onGetToken: " + JSONtoSend.toString());
                getter.execute(host + UrlAddDiscipline, JSONtoSend.toString());
            }

            @Override
            public void onFail() {
                callback.onFail("Ошибка получения токена");
            }
        });
    }

    public static void apiGetDiscipline(final Integer id, final ApiInterface.onCompleteGetDiscipline callback) {
        getToken(new onGetTokenListener() {
            @Override
            public void onGetToken(String token) {
                GetterJSON getter = new GetterJSON(new GetterJSON.onCompleteEventHandler() {
                    @Override
                    public void onComplete(String Json) {
                        if (Json == null) {
                            callback.onFail("Internet Error");
                            return;
                        }
                        try {
                            JSONObject result = new JSONObject(Json);
                            if (result.getInt("success") == 1) {
                                JSONArray jsonDiscipline = result.getJSONArray("disciplines");
                                ArrayList<Discipline> disciplines = new ArrayList<>();
                                Gson gson = builder.create();
                                for (int i = 0; i < jsonDiscipline.length(); i++) {
                                    disciplines.add(gson.fromJson(jsonDiscipline.get(i).toString(), Discipline.class));
                                }
                                callback.onSuccess(disciplines);
                            } else {
                                Log.d(TAG, "onComplete: " + result.toString());
                                callback.onFail(formatError(result.getJSONObject("error")));
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "apiGetCycle: exn " + e.getMessage());
                            callback.onFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onFail(String error) {
                        callback.onFail(error);
                    }
                });
                Gson gson = builder.create();
                JSONObject JSONtoSend = new JSONObject();
                try {
                    JSONtoSend.put("token", token);
                    Discipline discipline = new Discipline();
                    if (id != null) {
                        discipline.id = id;
                    }
                    JSONtoSend.put("discipline", new JSONObject(gson.toJson(discipline)));
                } catch (JSONException e) {
                    Log.e(TAG, "onClick: exn " + e.getMessage());
                    callback.onFail(e.getMessage());
                }
                Log.d(TAG, "onGetToken: " + JSONtoSend.toString());
                getter.execute(host + UrlDiscipline, JSONtoSend.toString());
            }

            @Override
            public void onFail() {
                callback.onFail("Ошибка получения ебаного токена");
            }
        });
    }

    public static void apiRemoveDiscipline(final Integer id, final ApiInterface.onComplete callback) {
        getToken(new onGetTokenListener() {
            @Override
            public void onGetToken(String token) {
                GetterJSON getter = new GetterJSON(new GetterJSON.onCompleteEventHandler() {
                    @Override
                    public void onComplete(String Json) {
                        if (Json == null) {
                            callback.onFail("Internet Error");
                            return;
                        }
                        try {
                            JSONObject result = new JSONObject(Json);
                            if (result.getInt("success") == 1) {
                                callback.onSuccess();
                            } else {
                                Log.d(TAG, "onComplete: " + result.toString());
                                callback.onFail(formatError(result.getJSONObject("error")));
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "apiGetCycle: exn " + e.getMessage());
                            callback.onFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onFail(String error) {
                        callback.onFail(error);
                    }
                });

                JSONObject JSONtoSend = new JSONObject();
                try {
                    JSONtoSend.put("token", token);
                    JSONObject jsonCycle = new JSONObject();
                    jsonCycle.put("id", id);
                    JSONtoSend.put("discipline", jsonCycle);
                } catch (JSONException e) {
                    Log.e(TAG, "onClick: exn " + e.getMessage());
                    callback.onFail(e.getMessage());
                }
                Log.d(TAG, "onGetToken: " + JSONtoSend.toString());
                getter.execute(host + UrlRemoveDiscipline, JSONtoSend.toString());
            }

            @Override
            public void onFail() {
                callback.onFail("ошибка получения токена");
            }
        });
    }

    private static String formatError(JSONObject error) throws JSONException {
        String message = "";
        message = message + "code: " + error.getInt("code") + "\n";
        message = message + "msg: " + error.getString("message");
        return message;
    }

    private static void getToken(final onGetTokenListener listener) {
        String token;
        token = SharedPreferencesHelper.getToken(mContext);
        if (token == null) {
            if (SharedPreferencesHelper.hasLogin(mContext) && SharedPreferencesHelper.hasPassword(mContext)) {
                apiLogin(SharedPreferencesHelper.getLogin(mContext), SharedPreferencesHelper.getPassword(mContext), new ApiInterface.onComplete() {
                    @Override
                    public void onSuccess() {
                        listener.onGetToken(SharedPreferencesHelper.getToken(mContext));
                    }

                    @Override
                    public void onFail(String error) {
                        listener.onFail();
                    }
                });
            } else
                listener.onFail();
        } else
            listener.onGetToken(token);
    }

    public interface onGetTokenListener {
        void onGetToken(String token);

        void onFail();
    }

    public static class ApiInterface {
        private interface onFail {
            void onFail(String error);
        }

        public interface onCompleteWithResult extends onFail {
            void onSuccess(String result);
        }

        public interface onComplete extends onFail {
            void onSuccess();
        }

        public interface onCompleteGetUser extends onFail {
            void onSuccess(User u);
        }

        public interface onCompleteGetCycle extends onFail {
            void onSuccess(ArrayList<Cycle> cycles);
        }

        public interface onCompleteGetDiscipline extends onFail {
            void onSuccess(ArrayList<Discipline> disciplines);
        }
    }
}
