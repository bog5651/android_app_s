package com.example.stas.android_app_s;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.stas.android_app_s.Activitys.LoginActivity;
import com.example.stas.android_app_s.Activitys.MainApp;
import com.example.stas.android_app_s.Helpers.RequestHelper;
import com.example.stas.android_app_s.Helpers.SharedPreferencesHelper;
import com.example.stas.android_app_s.Objects.User;

public class MainActivity extends AppCompatActivity {

    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RequestHelper.init(this);

        if (SharedPreferencesHelper.hasToken(this)) {
            RequestHelper.apiGetUser(new RequestHelper.ApiInterface.onCompleteGetUser() {
                @Override
                public void onSuccess(User u) {
                    Intent toStart = new Intent(context, MainApp.class);
                    toStart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                    startActivity(toStart);
                    finish();
                }

                @Override
                public void onFail(String error) {
                    Toast.makeText(context, "Сессия устарела, войдите снова", Toast.LENGTH_LONG).show();
                    Intent toStart = new Intent(context, LoginActivity.class);
                    toStart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                    startActivity(toStart);
                    finish();
                }
            });
        } else {
            Intent toStart = new Intent(context, LoginActivity.class);
            toStart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
            startActivity(toStart);
            finish();
        }
    }
}
