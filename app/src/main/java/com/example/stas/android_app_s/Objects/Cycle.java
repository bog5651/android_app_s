package com.example.stas.android_app_s.Objects;

import com.google.gson.annotations.SerializedName;

public class Cycle {
    @SerializedName("cycle_ID")
    public Integer id;

    @SerializedName("cycle_name")
    public String cycle_name;
}
