package com.example.stas.android_app_s.Objects;

import com.google.gson.annotations.SerializedName;

public class Discipline {
    @SerializedName("discipline_ID")
    public Integer id;

    @SerializedName("d_name")
    public String d_name;

    @SerializedName("semester")
    public Integer semester;

    @SerializedName("school_hours")
    public Integer school_hours;

    @SerializedName("d_cycle_ID")
    public Integer d_cycle_ID;
}
