package com.example.stas.android_app_s.Objects;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("id")
    public int id_user;

    @SerializedName("login")
    public String login;

    @SerializedName("firesname")
    public String firesname;

    @SerializedName("lastname")
    public String lastname;

    @SerializedName("birthdate")
    public String birthdate;

    @SerializedName("email")
    public String email;

    public String password;
}
